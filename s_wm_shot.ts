#!/usr/bin/env bun

/*
 * Bin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bin. If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

import process from "process";
import path from "path";
import os from "os";
import fs from "fs/promises";
import { exec } from "child_process";

// BUN
import { parseArgs } from "util";
// import { $ } from "bun";

let home_dir = os.homedir();
const shot_dir = path.join(home_dir, "Pictures", "screenshots");
let shot_name: string = path.join(shot_dir, `screenshot_${Date.now()}.png`);

try {
    await fs.access(shot_dir, fs.constants.R_OK | fs.constants.W_OK);
} catch (err: unknown) {
    if (err.code === "ENOENT") await fs.mkdir(shot_dir);
    else throw err;
}

let grimshot_info: Record<string, string[]> = {
    exec: [
        "/usr/bin/grimshot",
        path.join(home_dir, ".guix-profile", "bin", "grimshot"),
    ],
    full_copy: ["copy", "active"],
    full_save: ["save", "active"],
    partial_copy: ["copy", "area"],
    partial_save: ["save", "area"],
};

async function exec_present(shotter: string[]): Promise<string | null> {
    for (const e of shotter) {
        const c = await Bun.file(e).exists();
        if (c) return e;
    }

    return null;
}

async function shot_full(): Promise<string> {
    let shot_mode = values.copy
        ? grimshot_info["full_copy"]
        : grimshot_info["full_save"];
    const exec_name = await exec_present(grimshot_info["exec"]);

    return `${exec_name} ${shot_mode.join(" ")} ${shot_name}`;
}

async function shot_partial(): Promise<string> {
    let shot_mode = values.copy
        ? grimshot_info["partial_copy"]
        : grimshot_info["partial_save"];
    const exec_name = await exec_present(grimshot_info["exec"]);

    return `${exec_name} ${shot_mode.join(" ")} ${shot_name}`;
}

// CLI

const { values } = parseArgs({
    args: Bun.argv,
    options: {
        full: {
            type: "boolean",
        },
        partial: {
            type: "boolean",
        },
        copy: {
            type: "boolean",
        },
        help: {
            type: "boolean",
        },
        save: {
            type: "boolean",
        },
    },
    strict: true,
    allowPositionals: true,
});

function print_usage() {
    let usage_info = `usage: s_wm_shot [-h] [-f] [-p] [-s] [-c]

options:
  -h, --help     show this help message and exit
  -f, --full     full copy of current screenshot
  -p, --partial  partial copy of current screenshot
  -s, --save     partial copy of current screenshot
  -c, --copy     partial copy of current screenshot`;

    process.stdout.write(usage_info);
    process.exit(0);
}

if (values.full) {
    shot_full()
        .then((final_cmd) => exec(final_cmd))
        .catch((err) => process.stdout.write(`${err}`))
        .finally(() => process.exit(0));
}

if (values.partial) {
    shot_partial()
        .then((final_cmd) => exec(final_cmd))
        .catch((err) => process.stdout.write(`${err}`))
        .finally(() => process.exit(0));
}

if (Object.keys(values).length === 0 || values.help) {
    print_usage();
}
