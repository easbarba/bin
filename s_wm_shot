#!/usr/bin/env python3

# Bin is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Bin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bin. If not, see <https://www.gnu.org/licenses/>.

import subprocess
import argparse
from datetime import datetime
from pathlib import Path
import sys


shot_dir: Path = Path().home().joinpath("Pictures", "Screenshots")
if not shot_dir.exists():
    shot_dir.mkdir()


shot_name: str = "screenshot_" + datetime.now().strftime("%Y_%m_%d_%H_%M_%S") + ".png"


grimshot: dict[str, list[str]] = {
    "exec": [
        "/usr/bin/grimshot",
        Path().home().joinpath(".guix-profile", "bin", "grimshot"),
    ],
    "full_copy": ["copy", "active"],
    "full_save": ["save", "active"],
    "partial_copy": ["copy", "area"],
    "partial_save": ["save", "area"],
}

brighters: list[dict[str, list[str]]] = [grimshot]


def exec_found(execs: list[str]) -> str:
    "return executable found"

    for e in execs:
        if Path(e).exists():
            return e

    sys.exit(1)


def shot_full() -> None:
    """"""
    shot_mode: list[str] = grimshot["full_copy"] if args.copy else grimshot["full_save"]
    final_cmd: list[str] = (
        [exec_found(grimshot["exec"])] + shot_mode + [str(shot_dir.joinpath(shot_name))]
    )
    subprocess.run(final_cmd)


def shot_partial() -> None:
    """"""
    shot_mode: list[str] = (
        grimshot["partial_copy"] if args.copy else grimshot["partial_save"]
    )
    final_cmd: list[str] = (
        [exec_found(grimshot["exec"])] + shot_mode + [str(shot_dir.joinpath(shot_name))]
    )
    subprocess.run(final_cmd)


parser = argparse.ArgumentParser(prog="s_wm_shot", description="")
parser.add_argument(
    "-f", "--full", help="full copy of current screenshot", action="store_true"
)
parser.add_argument(
    "-p", "--partial", help="partial copy of current screenshot", action="store_true"
)
parser.add_argument(
    "-s", "--save", help="partial copy of current screenshot", action="store_true"
)
parser.add_argument(
    "-c", "--copy", help="partial copy of current screenshot", action="store_true"
)
args = parser.parse_args()


def main():
    """Go for it."""
    try:
        if args.full:
            shot_full()
            return

        if args.partial:
            shot_partial()
            return

        parser.print_help()
    except KeyboardInterrupt:
        print("User cancelled, exiting!")
        exit(1)


if __name__ == "__main__":
    main()
